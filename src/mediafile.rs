use crate::error::{Error, Result};
use log::debug;
use slugify::slugify;
use xml::escape::escape_str_attribute;

/// A media file to stream
#[derive(Debug, Clone)]
pub struct MediaFile {
    file_path: std::path::PathBuf,
    file_uri: String,
    content_type: String,
    content_type_ext: Option<String>,
}

impl std::fmt::Display for MediaFile {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "'{}': path: '{}', content-type: '{}{}'",
            self.file_uri,
            self.file_path.display(),
            self.content_type,
            match &self.content_type_ext {
                Some(ext) => format!(";{}", ext),
                None => "".to_string(),
            }
        )
    }
}

impl MediaFile {
    /// Create a new media file entry
    pub fn new(media_path: &std::path::Path) -> Result<Self> {
        debug!("Creating media file");
        let media_file = match media_path.exists() {
            true => {
                let file_path = media_path.to_path_buf();
                let (content_type, content_type_ext) =
                    match file_path.extension().unwrap().to_str().unwrap() {
                        "pcm16" => (
                            "audio/L16".to_string(),
                            Some("rate=44100;channels=2".to_string()),
                        ),
                        _ => (
                            mime_guess::from_path(media_path)
                                .first_or_octet_stream()
                                .to_string(),
                            None,
                        ),
                    };

                MediaFile {
                    file_path,
                    file_uri: slugify!(media_path.display().to_string().as_str(), separator = "."),
                    content_type,
                    content_type_ext,
                }
            }
            false => {
                return Err(Error::StreamingFileDoesNotExist(
                    media_path.display().to_string(),
                ));
            }
        };

        Ok(media_file)
    }

    pub fn get_file_path(&self) -> &std::path::PathBuf {
        &self.file_path
    }

    pub fn get_file_uri(&self) -> &String {
        &self.file_uri
    }

    pub fn get_content_type(&self) -> &String {
        &self.content_type
    }

    pub fn get_content_type_ext(&self) -> &Option<String> {
        &self.content_type_ext
    }

    #[doc(hidden)]
    pub fn media_uri_for_remote(&self, local_ip: &String, local_port: u32) -> String {
        format!(
            "{}/{}",
            self.host_uri_for_remote(local_ip, local_port),
            self.file_uri
        )
    }

    #[doc(hidden)]
    pub fn media_uri_metadata_for_remote(&self, local_ip: &String, local_port: u32) -> String {
        let host_uri = self.host_uri_for_remote(local_ip, local_port);
        escape_str_attribute(
            format!("\
                <?xml version='1.0' encoding='utf-8'?> \
                <DIDL-Lite xmlns=\"urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/\" \
                        xmlns:dc=\"http://purl.org/dc/elements/1.1/\" \
                        xmlns:dlna=\"urn:schemas-dlna-org:metadata-1-0/\" \
                        xmlns:sec=\"http://www.sec.co.kr/\" \
                        xmlns:upnp=\"urn:schemas-upnp-org:metadata-1-0/upnp/\"> \
                    <item id=\"0\" parentID=\"0\" restricted=\"1\"> \
                        <upnp:class>object.item.audioItem.musicTrack</upnp:class> \
                        <dc:title>MPlayer</dc:title> \
                        <dc:creator></dc:creator> \
                        <upnp:artist>Liveaudio on frederic-zen</upnp:artist> \
                        <upnp:albumArtURI>{}/</upnp:albumArtURI> \
                        <upnp:album></upnp:album> \
                        <res protocolInfo=\"http-get:*:{}:DLNA.ORG_OP=00;DLNA.ORG_CI=0;DLNA.ORG_FLAGS=01700000000000000000000000000000\">{}/{}</res> \
                    </item> \
                </DIDL-Lite>",
                host_uri,
                self.content_type,
                host_uri,
                self.file_uri
            ).as_str()
        ).to_string()
    }

    fn host_uri_for_remote(&self, local_ip: &String, local_port: u32) -> String {
        format!("http://{}:{}", local_ip, local_port)
    }
}
