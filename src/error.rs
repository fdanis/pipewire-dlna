// SPDX-FileCopyrightText: Copyright © 2023 Collabora Ltd.
// SPDX-License-Identifier: MIT
// Author: Frederic Danis <frederic.danis@collabora.com>

use std::fmt;

/// Errors that can happen inside pipewire-dlna
#[derive(Debug)]
pub enum Error {
    /// An error occurred while discovering devices
    DevicesDiscoverFail(rupnp::Error),
    /// An error occurred while iterating over discovered devices
    DevicesNextDeviceError(rupnp::Error),
    /// An error occurred parsing a host or IP address
    StreamingHostParseError(String),
    /// An error occurred when a certain media file does not exist
    StreamingFileDoesNotExist(String),
    /// An error occurred while trying to connect to the render
    StreamingRemoteRenderConnectFail(String, std::io::Error),
    /// An error occurred while trying to identify the host IP address
    StreamingIdentifyLocalAddressError(std::io::Error),
    /// An error occurred while sending the SetAVTransportURI DLNA action to the render
    DLNASetAVTransportURIError(rupnp::Error),
    /// An error occurred while sending the Play DLNA action to the render
    DLNAPlayError(rupnp::Error),
    /// An error occurred while serving and streaming the media files
    DLNAStreamingError(tokio::task::JoinError),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::DevicesDiscoverFail(err) => write!(f, "Failed to discover devices: {}", err),
            Error::DevicesNextDeviceError(err) => write!(f, "Failed to get next device: {}", err),
            Error::StreamingHostParseError(host) => write!(f, "Failed to parse host '{}'", host),
            Error::StreamingFileDoesNotExist(file) => write!(f, "File '{}' does not exist", file),
            Error::StreamingRemoteRenderConnectFail(host, err) => {
                write!(f, "Failed to connect to remote render '{}': {}", host, err)
            }
            Error::StreamingIdentifyLocalAddressError(err) => {
                write!(f, "Failed to identify local address: {}", err)
            }
            Error::DLNASetAVTransportURIError(err) => {
                write!(f, "Failed to set AVTransportURI: {}", err)
            }
            Error::DLNAPlayError(err) => write!(f, "Failed to Play: {}", err),
            Error::DLNAStreamingError(err) => write!(f, "Failed to stream: {}", err),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::DevicesDiscoverFail(err) => Some(err),
            Error::DevicesNextDeviceError(err) => Some(err),
            Error::StreamingRemoteRenderConnectFail(_, err) => Some(err),
            Error::StreamingIdentifyLocalAddressError(err) => Some(err),
            Error::DLNASetAVTransportURIError(err) => Some(err),
            Error::DLNAPlayError(err) => Some(err),
            Error::DLNAStreamingError(err) => Some(err),
            _ => None,
        }
    }
}

pub type Result<T, E = Error> = std::result::Result<T, E>;
