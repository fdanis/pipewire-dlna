// SPDX-FileCopyrightText: Copyright © 2023 Collabora Ltd.
// SPDX-License-Identifier: MIT
// Author: Frederic Danis <frederic.danis@collabora.com>

mod error;
mod mediafile;
mod streaming;

use crate::{
    error::{Error, Result},
    mediafile::MediaFile,
    streaming::{get_serve_ip, MediaStreamingServer},
};
use clap::Parser;
use futures::prelude::*;
use log::info;
use rupnp::ssdp::{SearchTarget, URN};
use std::env;
use std::sync::Arc;
use std::time::Duration;

const RENDERING_TRANSPORT: URN = URN::service("schemas-upnp-org", "AVTransport", 1);

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
struct Cli {
    media_path: std::path::PathBuf,

    /// Set the server ip
    #[clap(long, default_value = "0.0.0.0")]
    host: String,

    /// Set the server port
    #[clap(short, long, default_value_t = 8080)]
    port: u32,

    /// Turn debugging information on
    #[clap(short, long, action = clap::ArgAction::Count)]
    verbose: u8,
}

#[tokio::main]
async fn main() -> Result<(), crate::error::Error> {
    let cli = Cli::parse();
    let log_level = match cli.verbose {
        0 => "warning",
        1 => "info",
        2 => "debug",
        _ => "trace",
    };

    env::set_var("RUST_LOG", log_level);
    pretty_env_logger::init();

    let media_file = Arc::new(MediaFile::new(&cli.media_path)?);

    // Start streaming server
    let media_streaming_server =
        MediaStreamingServer::new(media_file.clone(), &cli.host, cli.port)?;
    let streaming_server_handle = tokio::spawn(async move { media_streaming_server.run().await });

    let search_target = SearchTarget::URN(RENDERING_TRANSPORT);
    let devices = rupnp::discover(&search_target, Duration::from_secs(3))
        .await
        .map_err(Error::DevicesDiscoverFail)?;
    pin_utils::pin_mut!(devices);

    while let Some(device) = devices
        .try_next()
        .await
        .map_err(Error::DevicesNextDeviceError)?
    {
        info!("found {} @ {}", device.friendly_name(), device.url());
        let local_ip = if cli.host == "0.0.0.0".to_string() {
            let target_host = device.url().authority().unwrap().host().to_string();
            get_serve_ip(&target_host).await?
        } else {
            cli.host.clone()
        };
        info!("local ip: {}", local_ip);

        let urn = URN::service(
            "schemas-upnp-org",
            "AVTransport",
            device.device_type().version(),
        );
        let transport = device.find_service(&urn).unwrap();

        let args = format!(
            r#"<InstanceID>0</InstanceID><CurrentURI>{}</CurrentURI><CurrentURIMetaData>{}</CurrentURIMetaData>"#,
            media_file.media_uri_for_remote(&local_ip, cli.port),
            media_file.media_uri_metadata_for_remote(&local_ip, cli.port)
        );
        match transport
            .action(device.url(), "SetAVTransportURI", args.as_str())
            .await
            .map_err(Error::DLNASetAVTransportURIError)
        {
            Ok(_) => {}
            Err(e) => {
                println!("{}: {:?}", device.friendly_name(), e);
                continue;
            }
        }

        let args = "<InstanceID>0</InstanceID><Speed>1</Speed>";
        match transport
            .action(device.url(), "Play", args)
            .await
            .map_err(Error::DLNAPlayError)
        {
            Ok(_) => println!("Start playing on {}", device.friendly_name()),
            Err(e) => println!("{:?}", e),
        }
    }

    streaming_server_handle
        .await
        .map_err(Error::DLNAStreamingError)?;

    Ok(())
}
