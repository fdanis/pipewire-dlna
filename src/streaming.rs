// SPDX-FileCopyrightText: Copyright © 2023 Collabora Ltd.
// SPDX-License-Identifier: MIT
// Author: Frederic Danis <frederic.danis@collabora.com>

use crate::{
    error::{Error, Result},
    mediafile::MediaFile,
};
use log::{debug, info};
use std::net::{Ipv4Addr, SocketAddr, UdpSocket};
use std::sync::Arc;
use warp::Filter;

const DUMMY_PORT: u32 = 0;

/// A media streaming server
#[derive(Debug, Clone)]
pub struct MediaStreamingServer {
    media_file: Arc<MediaFile>,
    server_addr: SocketAddr,
}

impl MediaStreamingServer {
    /// Create a new media streaming server
    pub fn new(media_file: Arc<MediaFile>, addr: &String, port: u32) -> Result<Self> {
        let server_addr: SocketAddr = format!("{}:{}", addr, port)
            .parse()
            .map_err(|_| Error::StreamingHostParseError(addr.to_string()))?;

        debug!("Streaming server address: {}", server_addr);
        Ok(Self {
            media_file,
            server_addr,
        })
    }

    /// Start the media streaming server.
    pub async fn run(&self) {
        let media_file = self.media_file.clone();
        let media_route = warp::get()
            .and(warp::path!(String).and_then(move |name| stream_file(name, media_file.clone())));
        warp::serve(media_route).run(self.server_addr).await;
    }
}

async fn stream_file(
    id: String,
    media_file: Arc<MediaFile>,
) -> Result<impl warp::Reply, warp::Rejection> {
    if id != *media_file.get_file_uri() {
        debug!("Unsupported file: {}", id);
        return Err(warp::reject::reject());
    }

    info!("Serving media file: {}", media_file);

    tokio::fs::File::open(media_file.get_file_path())
        .await
        .map(|file| tokio_util::codec::FramedRead::new(file, tokio_util::codec::BytesCodec::new()))
        .map(hyper::Body::wrap_stream)
        .map(|body| {
            hyper::Response::builder()
                .header(
                    "Content-Type",
                    match media_file.get_content_type_ext() {
                        Some(ext) => format!("{};{}", media_file.get_content_type(), ext),
                        None => media_file.get_content_type().clone(),
                    },
                )
                .body(body)
        })
        .map_err(to_rejection)
}

fn to_rejection<E>(_e: E) -> warp::Rejection {
    warp::reject::reject()
}

/// Identifies the local serve IP address according to a target host.
pub async fn get_serve_ip(target_host: &String) -> Result<String> {
    debug!(
        "Identifying local serve IP for target host: {}",
        target_host
    );
    let target_addr: SocketAddr = format!("{}:{}", target_host, DUMMY_PORT)
        .parse()
        .map_err(|_| Error::StreamingHostParseError(target_host.to_owned()))?;

    let socket = UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0))
        .map_err(|err| Error::StreamingRemoteRenderConnectFail(target_addr.to_string(), err))?;
    socket
        .connect(target_addr)
        .map_err(|err| Error::StreamingRemoteRenderConnectFail(target_addr.to_string(), err))?;

    Ok(socket
        .local_addr()
        .map_err(Error::StreamingIdentifyLocalAddressError)?
        .ip()
        .to_string())
}
